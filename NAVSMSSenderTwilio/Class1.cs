﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace NAVSMSSenderTwilio
{
    public class TwilioNAV
    {
        private string AuthSID;
        private string AuthToken;
        private string FromPhoneNo;

        public void SendMessage(String phonenumber, String textmessage)
        {
            TwilioClient.Init(AuthSID, AuthToken);


            try
            {
                var message = MessageResource.Create(
                    from: new Twilio.Types.PhoneNumber(FromPhoneNo),
                    body: textmessage,
                    to: new Twilio.Types.PhoneNumber(phonenumber)
                );
            }
            catch
            {

            }

        }

        public void SetAuthSID(String pAuthSID)
        {
            AuthSID = pAuthSID;
        }
        public void SetAuthToken(String pAuthToken)
        {
            AuthToken = pAuthToken;
        }
        public void SetFromPhoneNo(String pFromPhoneNo)
        {
            FromPhoneNo = pFromPhoneNo;
        }
    }
}
